# Notifier

## Usage

```go
package main
import (
"bitbucket.org/InvolvedPanda/notifier"
"log"
"os"
)

func main() {
    n := notifier.New(os.Getenv("KAFKA_SERVER"), os.Getenv("NOTIFY_TOPIC"))
    err := n.Notify(notifier.TrxTransformer{
                Id: 1,
                EndToEndId:"asd",
                Type:"Payment",
                Direction:"Inbound",
                Amount: 10,
                Currency:"EUR",
                Status:"Created",
                Updated_at:"2020-01-02 12:12:12",
    })
    if err != nil {
        log.Fatal(err)
    }

    err = n.Close()
    if err != nil {
    	log.Fatal(err)
    }
}
```