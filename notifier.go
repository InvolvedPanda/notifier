package notifier

import (
	"encoding/json"
	"log"
)

type Notificator struct {
	w *writer
}

func New(kafkaServer, topic string) *Notificator {
	return &Notificator{w: newWriter(kafkaServer, topic)}
}

func (n *Notificator) Notify(transformer TrxTransformer) error {
	bytes, err := json.Marshal(transformer)
	if err != nil {
		return err
	}
	return n.w.writeMessage(bytes)
}

func (n *Notificator) NotifyBatch(transformers ...TrxTransformer) error {
	batch := [][]byte{}

	for _, trnr := range transformers {
		bytes, err := json.Marshal(trnr)
		if err != nil {
			log.Println(err)
			continue
		}
		batch = append(batch, bytes)
	}

	return n.w.writeMessages(batch...)
}

func (n *Notificator) Close() error {
	return n.w.close()
}
