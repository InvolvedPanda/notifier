package notifier

type TrxTransformer struct {
	Id         int64  `json:"trx_id"`
	EndToEndId string `json:"end_to_end_id"`
	Type       string `json:"type"`
	Direction  string `json:"direction"`
	Amount     int64  `json:"amount"`
	Currency   string `json:"currency"`
	Status     string `json:"status"`
	Updated_at string `json:"updated_at"`
}
