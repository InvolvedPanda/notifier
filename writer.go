package notifier

import (
	"context"
	"github.com/segmentio/kafka-go"
	"log"
	"time"
)

type writer struct {
	writer *kafka.Writer
	topic  string
}

func newWriter(kafkaServer, topic string) *writer {
	writeConfig := kafka.WriterConfig{
		Brokers:  []string{kafkaServer},
		Topic:    topic,
		Balancer: &kafka.RoundRobin{},
		BatchTimeout: 1 * time.Millisecond,
	}
	return &writer{writer: kafka.NewWriter(writeConfig), topic: topic}
}

func (writer *writer) close() error {
	return writer.writer.Close()
}

func (writer *writer) writeMessage(message []byte) error {
	log.Println("sending a message to topic " + writer.topic + " message: " + string(message))
	return writer.writer.WriteMessages(context.Background(), kafka.Message{
		Key:   []byte("Body"),
		Value: message,
	})
}

func (writer *writer) writeMessages(messages ...[]byte) error {
	batch := [] kafka.Message{}
	for _, msg := range messages {
		log.Println("sending a messages to topic " + writer.topic + " message: " + string(msg))
		batch = append(batch, kafka.Message{
			Key:   []byte("Body"),
			Value: msg,
		})
	}

	err := writer.writer.WriteMessages(context.Background(), batch...)
	log.Printf("%+v\n", writer.writer.Stats())

	return err
}
